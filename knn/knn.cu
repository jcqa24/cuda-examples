#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include "knn.h"

float **arr;


void Intercambia(float **arr, int ini, int mitad, int fin,int r) {
   	int i, j, k;
   	int b[r];
   	int a[r];
   	for(i = ini, j = mitad + 1, k = ini; i <= mitad && j <= fin; k++) {
    	if(arr[1][i] <= arr[1][j]){
    		b[k] = arr[1][i];
    		a[k] = arr[0][i];
    		i++;	
    	}else{
         	b[k] = arr[1][j];
         	a[k] = arr[0][j];
         	j++;
      	}
    }
   	while(i <= mitad) {
   		b[k] = arr[1][i];
   		a[k] = arr[0][i];
   		k++;
   		i++;
   	}         	
   	while(j <= fin){
   		b[k] = arr[1][j];
   		a[k] = arr[0][j];
   		k++;
   		j++;
   	}   
   	for(k = ini; k <= fin; k++){
      	arr[1][k] = b[k];
      	arr[0][k] = a[k];
    }
}

void MergeSortRec(float **arr, int ini, int fin,int r){
	int mitad;
	if(ini < fin){
		mitad = ((ini +fin)/2);
		MergeSortRec(arr, ini, mitad,r);
		MergeSortRec(arr, (mitad+1), fin,r);
		Intercambia(arr, ini, mitad, fin,r);
	}
}


int * knn(int ** DatosE,int **DatosT,int *Classes,int nclases,int r,int c,int k){
	int * Clasifiacion = (int*)malloc(r*sizeof(int));
	arr = (float **)(malloc(2 * sizeof(float *)));
	for (int i = 0; i < r; i++)
	{
		arr[i]= (float *)(malloc(r* sizeof(float*)));
	}
	
    for (int i = 0; i < r; i++)
    {

        // Calculo de distancias
        for (int j = 0; j < r; j++)
        {
            
            arr[0][j]=DatosE[j][c-1];
            arr[1][j] = Distance(DatosE[j],DatosT[i],c);
        }

		printf("\nOrdenando...\n");

		MergeSortRec(arr,0,r-1,r);

		for (int j = 0; j < r; j++)
        {
			printf("La distacia es: %f \t",arr[1][j]);
			printf("La clase es: %f \n",arr[0][j]);
		}
		int count;
		int classmax =0;
		int nmax = 0;

		//votacion

		for (int i = 0; i < nclases; i++)
		{	
			count = 0;
			for (int j = 0; j <= k; j++)
			{
				if (arr[0][j] == Classes[i])
				{
					count ++;
				}
				
			}
			if (count> nmax){
				nmax = count;
				classmax=Classes[i];
			}
			
		}

		Clasifiacion[i]=classmax;
		printf("La clasificacion es: %d\n",Clasifiacion[i]);
		
        
        
    }
    

    return Clasifiacion;
}


float Distance(int *CE, int *CP,int c){
    float d = 0;

    for (int i = 0; i <c-1; i++)
    {   
        d += pow(CE[i] - CP[i], 2);
    }
    
    d = sqrt(d);

    return d;

}


