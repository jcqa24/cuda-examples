#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "knn.h"

int main()
{

    char *nameFileTraining = (char *)malloc(100 * sizeof(char *));
    FILE *FT;
    int **Datos = (int **)malloc(24 * sizeof(int *));
    char *word = (char *)malloc(20 * sizeof(char));
    char *lines = (char *)malloc(100 * sizeof(char));
    printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
    printf("\n\t\tSistema de clasificacion KNN\n\n");
    printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");

    //printf("\n\nProporcione el nombre del archivo de entrenamiento\n");

    //scanf("%s", nameFileTraining);

    for (int i = 0; i < 24; i++)
    {
        Datos[i] = (int *)malloc(5 * sizeof(int));
    }
    int *Clasifiacion = (int *)malloc(24 * sizeof(int));
    int col = 0;
    int row = 0;
    int suma;
    nameFileTraining = "lentes_entrenamiento.dat";
    FT = fopen(nameFileTraining, "r");
    while (fgets(lines, 100 * sizeof(char), (FILE *)FT))
    {
        word = strtok(lines, ",");
        while (word != NULL)
        {
            //printf("%s\t",word);
            suma = 0;
            for (int i = 0; i < strlen(word); i++)
            {
                suma += word[i];
            }
            Datos[row][col] = suma;
            col++;
            word = strtok(NULL, ",");
        }
        col = 0;
        row++;
    }

    /*     for (int i = 0; i < 24; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            printf("%d\t",Datos[i][j]);
        }
        printf("\n");
        
    }
     */
    int *Clases = (int *)malloc(3 * sizeof(int));

    Clases[0] = 738;
    Clases[1] = 717;
    Clases[2] = 535;



    int total = 0;
    double accurry = 0.0;

    Clasifiacion = knn(Datos, Datos, Clases, 3, 24, 5, 7);
   

    for (int i = 0; i < 24; i++)
    {
        printf("clasificacion %d\t\t Original %d\n",Clasifiacion[i],Datos[i][4]);
        if(Clasifiacion[i] == Datos[i][4]){
            total ++;
          
        }
     
    }
    printf("%d\n",total);
    accurry = (total/24.0) * 100;
    printf("La exactitud es: %f\n",accurry);

    return 0;
}
