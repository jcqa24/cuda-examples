#include<stdio.h>
#include<stdlib.h>
#include <cuda.h>


//Kernel function
//Parallel 
__global__ void hello() {
    printf("Hello world from device\n");
}



int main() {
    //N Grups, N Treads
    hello<<<1, 1>>>();
    cudaDeviceSynchronize();
    printf("Hello world from host\n");
    return 0;
}
