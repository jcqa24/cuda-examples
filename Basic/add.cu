#include<stdio.h>
#include<stdlib.h>
#include<cuda.h>

// Kernel definition
__global__ void VecAdd(int* A, int* B, int* C)
{
    
    int i = threadIdx.x;	
    printf("Soy el hilo %i\n",i);
    C[i] = A[i] + B[i];
    printf("%d  +  %d  => %d",A[i],B[i],C[i]);
	
}


void PrintArr(char X, int *A,int N){
	printf("%c =>  [",X);
	for (int i=0; i < N; i++){
		printf(" %d,",A[i]);
	}
	printf("\n");
}

int main(){
	
	int N = 10;
	int size = N * sizeof(int);
	//h -> host dispositivo base
	int *Ah =  (int*) malloc(size); 
	int *Bh =  (int*) malloc(size); 
	int *Ch =  (int*) malloc(size); 
	
	for (int i=0; i < N; i++){
		Ah[i] = rand() % (N+1);
		Bh[i] = rand() % (N+1);
	} 

	PrintArr('A',Ah,N);

	PrintArr('B',Bh,N);

	//asignar memoria en la GPU
	// g -> memoria GPU
	int *Ag;
	int *Bg;
	int *Cg;

	cudaMalloc(&Ag,size);
	cudaMalloc(&Bg,size);
	cudaMalloc(&Cg,size);

	cudaMemcpy(Ag,Ah,size,cudaMemcpyHostToDevice);
	cudaMemcpy(Bg,Bh,size,cudaMemcpyHostToDevice);
	// call kenernel parall
	VecAdd<<<1, N>>>(Ag, Bg, Cg);
	cudaDeviceSynchronize();

	//VecAdd(A,B,C,N);

	cudaMemcpy (Ch, Cg, size, cudaMemcpyDeviceToHost);
	PrintArr('C',Ch,N);
	cudaFree(Ag);
	cudaFree(Bg);
	cudaFree(Cg);

	return 0;
}
